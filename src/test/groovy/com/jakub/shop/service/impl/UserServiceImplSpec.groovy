package com.jakub.shop.service.impl

import com.jakub.shop.domain.dao.User
import com.jakub.shop.repository.RoleRepository
import com.jakub.shop.repository.UserRepository
import org.springframework.data.domain.PageRequest
import org.springframework.security.crypto.password.PasswordEncoder
import spock.lang.Specification

import javax.persistence.EntityNotFoundException

class UserServiceImplSpec extends Specification {
    def userService
    def userRepository = Mock(UserRepository)
    def passwordEncoder = Mock(PasswordEncoder)
    def roleRepository = Mock(RoleRepository)

    def setup() {
        userService = new UserServiceImpl(userRepository, passwordEncoder, roleRepository)
    }

    def 'should get user by id'() {
        when:
        userService.findById(1L)

        then:
        1 * userRepository.findById(1L) >> Optional.of(new User())
        0 * _
    }

    def 'should throw exception when user does not exist'() {
        given:
        userRepository.findById(1L) >> Optional.empty()

        when:
        userService.findById(1L)

        then:
        thrown EntityNotFoundException
    }

    def 'should save user'() {
        given:
        def user = new User()

        when:
        userService.save(user)

        then:
        1 * roleRepository.findByName('ROLE_USER') >> Optional.empty()
        1 * passwordEncoder.encode(user.getPassword())
        1 * userRepository.save(user)
        0 * _
    }

    def 'should delete user'() {
        when:
        userService.deleteById(1l)

        then:
        1 * userRepository.deleteById(1l)
        0 * _
    }

    def 'should return user page'() {
        given:
        def pageable = PageRequest.of(1, 5)

        when:
        userService.getPage(pageable)

        then:
        1 * userRepository.findAll(pageable)
        0 * _
    }
}

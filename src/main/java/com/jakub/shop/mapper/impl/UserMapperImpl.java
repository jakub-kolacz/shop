//package com.jakub.shop.mapper.impl;
//
//import com.jakub.shop.domain.dao.User;
//import com.jakub.shop.domain.dto.UserDto;
//import com.jakub.shop.mapper.UserMapper;
//import org.springframework.data.domain.Page;
//import org.springframework.stereotype.Component;
//
//@Component
//public class UserMapperImpl implements UserMapper {
//    @Override
//    public UserDto toDto(User user) {
//        return UserDto.builder()
//                .id(user.getId())
//                .email(user.getEmail())
//                .firstName(user.getFirstName())
//                .lastName(user.getLastName())
//                .build();
//    }
//
//    @Override
//    public User toDao(UserDto userDto) {
//        return User.builder()
//                .id(userDto.getId())
//                .firstName(userDto.getFirstName())
//                .lastName(userDto.getLastName())
//                .email(userDto.getEmail())
//                .password(userDto.getPassword())
//                .build();
//    }
//
//    @Override
//    public Page<UserDto> toUserDtoPage(Page<User> users) {
//        return users.map(this::toDto);
//    }
//
//
//}

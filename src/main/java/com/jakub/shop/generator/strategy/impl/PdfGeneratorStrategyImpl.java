package com.jakub.shop.generator.strategy.impl;

import com.jakub.shop.generator.model.FileType;
import com.jakub.shop.generator.strategy.GeneratorStrategy;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class PdfGeneratorStrategyImpl implements GeneratorStrategy {

    @Override
    public FileType getType() {
        return FileType.PDF;
    }

    @Override
    public byte[] generateFile() {
        log.info("PDF");
        return new byte[0];
    }
}

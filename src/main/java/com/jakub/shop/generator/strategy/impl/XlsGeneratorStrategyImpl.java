package com.jakub.shop.generator.strategy.impl;

import com.jakub.shop.generator.model.FileType;
import com.jakub.shop.generator.strategy.GeneratorStrategy;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class XlsGeneratorStrategyImpl implements GeneratorStrategy {

    @Override
    public FileType getType() {
        return FileType.XLS;
    }

    @Override
    public byte[] generateFile() {
        log.info("XLS");
        return new byte[0];
    }
}

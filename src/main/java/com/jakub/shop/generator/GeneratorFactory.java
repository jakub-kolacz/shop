package com.jakub.shop.generator;

import com.jakub.shop.generator.model.FileType;
import com.jakub.shop.generator.strategy.GeneratorStrategy;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

@RequiredArgsConstructor
@Component
public class GeneratorFactory {
    private final List<GeneratorStrategy> strategies;
    private Map<FileType, GeneratorStrategy> strategyMap;

    @PostConstruct
    void init(){
        strategyMap = strategies.stream().collect(Collectors.toMap(GeneratorStrategy::getType, Function.identity()));
    }

    public GeneratorStrategy findStrategyByKey(FileType key){
        return strategyMap.get(key);
    }
}

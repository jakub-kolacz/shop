package com.jakub.shop.service;

import com.jakub.shop.domain.dao.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface UserService {
    User save(User user);

    User findById(Long id);

    User update(User user, Long id);

    void deleteById(Long id);

    Page<User> getPage(Pageable pageable);

    User getCurrentUser();
}

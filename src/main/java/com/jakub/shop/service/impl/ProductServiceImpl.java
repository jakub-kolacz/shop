package com.jakub.shop.service.impl;

import com.jakub.shop.domain.dao.Product;
import com.jakub.shop.repository.ProductRepository;
import com.jakub.shop.service.ProductService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import javax.transaction.Transactional;

@RequiredArgsConstructor
@Service
@Slf4j
public class ProductServiceImpl implements ProductService {
    public final ProductRepository productRepository;

    @Override
    public Product save(Product product) {
        return productRepository.save(product);
    }

    @Override
    public Product findById(Long id) {
        log.info("Product not in cache id:{}", id);
        return productRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException("Product with provided id: " + id + " doesn't exist"));
    }


    @Override
    @Transactional
    public Product update(Product product, Long id) {
        Product productDb = findById(id);
        productDb.setId(product.getId());
        productDb.setName(product.getName());
        productDb.setPrice(product.getPrice());
        productDb.setQuantity(product.getQuantity());
        return productDb;
    }

    @Override
    public void deleteById(Long id) {
        productRepository.deleteById(id);
    }

    @Override
    public Page<Product> getPage(Pageable pageable) {
        return productRepository.findAll(pageable);
    }
}

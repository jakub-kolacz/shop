package com.jakub.shop.service.impl;

import com.jakub.shop.domain.dao.Basket;
import com.jakub.shop.domain.dao.Product;
import com.jakub.shop.domain.dao.User;
import com.jakub.shop.exception.QuantityNotEnoughException;
import com.jakub.shop.repository.BasketRepository;
import com.jakub.shop.service.BasketService;
import com.jakub.shop.service.ProductService;
import com.jakub.shop.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class BasketServiceImpl implements BasketService {
    private final BasketRepository basketRepository;
    private final UserService userService;
    private final ProductService productService;

    @Override
    public Basket addToBasket(Product product) {
        Product productDb = productService.findById(product.getId());

        if (productDb.getQuantity() <= product.getQuantity()) {
            throw new QuantityNotEnoughException("Not enough quantity of product in database");
        }

        return basketRepository.save(Basket.builder()
                .quantity(product.getQuantity())
                .product(productDb)
                .user(userService.getCurrentUser())
                .build());
    }

    @Override
    @Transactional
    public void removeProductFromBasket(Long productId) {
        basketRepository.deleteByProductIdAndUserId(productId, userService.getCurrentUser().getId());
    }

    @Override
    public Basket updateProductQuantityInBasket(Product product) {
        User currentUser = userService.getCurrentUser();
        basketRepository.findByProductIdAndUserId(product.getId(),currentUser.getId())
                .ifPresentOrElse(basket -> {
                    if(product.getQuantity() <= productService.findById(product.getId()).getQuantity()){
                        basket.setQuantity(product.getQuantity());
                        basketRepository.save(basket);
                    }
                    throw new QuantityNotEnoughException("Not enough quantity to increase");
                }, () -> addToBasket(product));
        return basketRepository.findByProductIdAndUserId(product.getId(), currentUser.getId()).orElseThrow();
    }

    @Override
    public List<Product> getBasket() {
        Long userId = userService.getCurrentUser().getId();

        return basketRepository.findByUserId(userId).stream()
                .map(basket -> {
                    Product product = basket.getProduct();
                    product.setQuantity(basket.getQuantity());
                    return product;
                })
                .collect(Collectors.toList());
    }
}

package com.jakub.shop.controller;

import com.jakub.shop.generator.GeneratorFactory;
import com.jakub.shop.generator.model.FileType;
import com.jakub.shop.generic.GenericFactory;
import com.jakub.shop.generic.strategy.file.FileGeneratorStrategy;
import com.jakub.shop.generic.strategy.mail.MailStrategy;
import com.jakub.shop.generic.strategy.mail.model.MailType;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/files")
public class FileController {
    private final GeneratorFactory generatorFactory;
    private final GenericFactory<MailType, MailStrategy> mailFactory;
    private final GenericFactory<FileType, FileGeneratorStrategy> fileFactory;

    @GetMapping()
    public void testFactoryNotGeneric(@RequestParam FileType fileType) {
        generatorFactory.findStrategyByKey(fileType).generateFile();
    }

    @GetMapping("/filetype")
    public void testFactory(@RequestParam FileType fileType) {
        fileFactory.findStrategyByKey(fileType).generateFile();
    }

    @GetMapping("/mail/register")
    public void testFactory(@RequestParam MailType mailType) {
        mailFactory.findStrategyByKey(mailType).sendMail();
    }

}

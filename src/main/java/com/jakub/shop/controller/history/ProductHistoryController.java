package com.jakub.shop.controller.history;

import com.jakub.shop.domain.dto.ProductDto;
import com.jakub.shop.mapper.history.ProductHistoryMapper;
import com.jakub.shop.repository.ProductRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/history/products")
@RequiredArgsConstructor
public class ProductHistoryController {
    private final ProductHistoryMapper productHistoryMapper;
    private final ProductRepository productRepository;

    @GetMapping("/{id}")
    public Page<ProductDto> getProductHistoryPage(@RequestParam int page, @RequestParam int size, @PathVariable Long id) {
        return productRepository.findRevisions(id, PageRequest.of(page, size)).map(productHistoryMapper::toDto);
    }
}

package com.jakub.shop.controller;

import com.jakub.shop.domain.dto.ProductDto;
import com.jakub.shop.mapper.ProductMapper;
import com.jakub.shop.service.ProductService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/products")
@RequiredArgsConstructor
public class ProductController {
    private final ProductService productService;
    private final ProductMapper productMapper;

    @GetMapping("/{id}")
    public ProductDto getProductById(@PathVariable Long id){
        return productMapper.toDto(productService.findById(id));
    }

    @PostMapping
    @PreAuthorize("hasRole('ADMIN')")
    public ProductDto saveProduct(@RequestBody @Valid  ProductDto product){
        return  productMapper.toDto(productService.save(productMapper.toDao(product)));
    }

    @PutMapping("/{id}")
    public ProductDto updateProduct(@RequestBody @Valid ProductDto product,@PathVariable Long id){

        return  productMapper.toDto(productService.update(productMapper.toDao(product), id));
    }

    @GetMapping
    public Page<ProductDto> getProductPage(@RequestParam int page, @RequestParam int size){
        return productService.getPage(PageRequest.of(page, size)).map(productMapper::toDto);
    }

    @DeleteMapping("/{id}")
    public void deleteProduct(@PathVariable Long id){
        productService.deleteById(id);
    }

}

package com.jakub.shop.generic.strategy.file;

import com.jakub.shop.generator.model.FileType;
import com.jakub.shop.generic.strategy.GenericStrategy;

public interface FileGeneratorStrategy extends GenericStrategy <FileType> {
    byte[] generateFile();
}

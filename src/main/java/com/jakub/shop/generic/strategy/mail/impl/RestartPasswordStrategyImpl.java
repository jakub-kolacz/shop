package com.jakub.shop.generic.strategy.mail.impl;

import com.jakub.shop.generic.strategy.mail.MailStrategy;
import com.jakub.shop.generic.strategy.mail.model.MailType;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class RestartPasswordStrategyImpl implements MailStrategy {
    @Override
    public MailType getType() {
        return MailType.RESTART_PASSWORD;
    }

    @Override
    public void sendMail() {
        log.info("Restart_Password");
    }
}
